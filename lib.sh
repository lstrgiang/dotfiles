#!/usr/bin/env bash

# Echo
RESET='\033[0m'
BLACK='\033[0;30m'
DARK_GRAY='\033[1;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
BROWN='\033[0;33m'
CYAN='\033[0;36m'
WHITE='\033[1;37m'
PURPLE='\033[0;35m'
NC='\033[0m' # No Colors

function ok() {
    printf "${GREEN}[ok]${NC} $1\n"
}

function say() {
    printf "${PURPLE}[o.o]${NC} $1\n"
}

function trying() {
    printf "${CYAN}[doing]${NC} - $1\n"
}

function work() {
    printf "${PURPLE}[work]${NC} - $1\n"
}

function warn() {
    printf "${BROWN}[warning]${NC} - $1\n"
}

function error() {
    printf "${RED}[error]${NC} - $1\n"
}

# Function list
# Parse lines of file to array
parseLinesToArray() {
    array=() # Create array
    while IFS= read -r line # Read a line
    do
        array+=("$line") # Append line to the array
    done < "$1"
}
