#!/usr/bin/env bash

source ./lib.sh

trying "Checking homebrew install"
brew_bin=$(which brew) 2>&1 > /dev/null
if [[ $? != 0 ]]; then
  trying "Installing homebrew"
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    if [[ $? != 0 ]]; then
      error "Unable to install homebrew, $0 abort!"
      exit 2
    fi
else
    ok
    trying "Updating homebrew"
    brew update
    ok
    # Upgrade any already-installed formulae
    work "upgrade brew packages..."
    brew upgrade
    ok "brews updated..."
fi

trying "Installing zsh"
brew install zsh
ok

#echo "Installing oh-my-zsh"
#sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

work "Brew tap"
parseLinesToArray "./brew/tap-list.txt"
for e in "${array[@]}"
do
    trying "Tapping $e"
    brew tap "$e"
    if [[ $? != 0 ]]; then
        error "Tapping error: $0"
    else
    ok
    fi
done


#echo "Set default shell to zsh"
#sudo -s 'echo /usr/local/bin/zsh >> /etc/shells' && chsh -s /usr/local/bin/zsh
